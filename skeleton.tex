% Please make sure you insert your
% data according to the instructions in PoSauthmanual.pdf
\documentclass{PoS}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{bbold}

\title{Towards the P-wave nucleon-pion scattering amplitude in the $\Delta (1232)$ channel: interpolating fields and spectra}

\ShortTitle{P-wave nucleon-pion scattering amplitude in the $\Delta (1232)$ channel}
%\thanks{A footnote may follow.}
\author{\speaker{G. Silvi}$^{a,b}$, C. Alexandrou$^{c,d}$, G. Koutsou$^{d}$, S. Krieg$^{a,b}$, L. Leskovec$^{j}$, S. Meinel$^{e,f}$, J. Negele$^{g}$, S. Paul$^{b,d}$, M. Petschlies$^{h}$, A. Pochinsky$^{g}$, G. Rendon$^{e}$, S. Syritsyn$^{f,i}$\\		
		
        $^a$Institute for Advanced Simulation, Forschungszentrum J{\"u}lich GmbH, J{\"u}lich 52425, Germany\\
        $^b$Faculty of Mathematics und Natural Sciences, University of Wuppertal, Wuppertal-42119, Germany \\
        $^c$Department of Physics, University of Cyprus, POB 20537, 1678 Nicosia, Cyprus \\
        $^d$Computation-based Science and Technology Research Center, The Cyprus Institute, 20 Kavafi Str., Nicosia, 2121, Cyprus \\
        $^e$Department of Physics, University of Arizona, Tucson, AZ 85721, USA \\
        $^f$RIKEN BNL Research Center, Brookhaven National Laboratory, Upton, NY 11973, USA \\
        $^g$Center for Theoretical Physics, Massachusetts Institute of Technology
Cambridge, MA 02139, USA \\
        $^h$Helmholtz-Institut f{\"u}r Strahlen- und Kernphysik, University of Bonn, Bonn 53115, Germany\\
        $^i$Department of Physics and Astronomy, Stony Brook University, Stony Brook, NY 11794, USA\\
        $^j$Theory Center, Jefferson Lab, Newport News, VA 23606, USA \\
        
        
        E-mail: \email{g.silvi@fz-juelich.de}, \email{alexand@ucy.ac.cy}, \email{g.koutsou@cyi.ac.cy}, \email{sfkr@gmx.de}, \email{leskovec@jlab.org}, \email{smeinel@email.arizona.edu}, \email{negele@mit.edu}, \email{s.paul@hpc-leap.eu}, \email{marcus.petschlies@hiskp.uni-bonn.de}, \email{avp@mit.edu}, \email{jgrs@email.arizona.edu}, \email{ssyritsyn@quark.phy.bnl.gov}}



\abstract{The study of strong scattering in Lattice QCD is enabled by the use of the L{\"u}scher method, which defines a mapping between the two body spectrum in the finite volume and the infinite volume scattering amplitude. It however requires full and precise knowledge of the spectrum in a given moving frame and irreducible representation. In this project we investigate the $\Delta (1232)$ resonance in the pion-nucleon system. The focus of the talk is on the group theoretical construction of single and multi hadron interpolating fields in various moving frames and irreducible representations. We construct a varied basis of interpolating fields in all of the relevant irreducible representations and determine the relevant energy levels.}

\FullConference{The 36th Annual International Symposium on Lattice Field Theory - LATTICE2018\\
		22-28 July, 2018\\
		Michigan State University, East Lansing, Michigan, USA.}


\begin{document}
\section{Introduction}
 In nature the Delta baryons, also known as Delta resonances, are a family of subatomic particles composed of up and down quarks. The instability of baryons resonances makes them quite difficult to study their properties using just models. On the other hand, Lattice QCD and its first principle calculations makes it an excellent tool for studying hadron spectroscopy. In recent years the field has made big steps in the study of hadronic resonances (a review can be found in \cite{Briceno:2017max}). An approach known as "L{\"u}scher method" enables the access to hadron scattering amplitudes thorugh their relation with QCD eigenstates in the finite volume. We explore the nucleon-pion scattering in the $I=3/2$ and $J^P=3/2^+$ channel where the $\Delta(1232)$ is located. It decays predominantly to the stable nucleon-pion system in orbital angular momentum $L=1$ with a decay width of $\Gamma_{\Delta \rightarrow N\pi} \approx 117$ MeV. The process is almost completely elastic \cite{shrestha2012multichannel}, but nearby inelastic resonances with similar quantum number could have a small contribution on the phase shift that needs to be taken into account in the analysis. The lattice computation involves calculation of two point correlation functions between the QCD-stable interacting fields: pion, nucleon, and the interpolating field with the quantum numbers of the resonance. The correlators are built from a combination of smeared forward, sequential and stochastic propagators. We performed calculations on the \texttt{A7} ensemble (cf.~Table \ref{tab:BMWconfigs}), where preliminary results on $3072$ measurements were analyzed.
 
We make use of a a tree-level improved Symanzik gauge action together with tree-level clover-improved Wilson fermion. The gauge links in the fermion action undergo two levels of HEX smearing as explained in \cite{Durr:2010aw}.

\begin{table}[!htb]
\begin{center}
\begin{tabular}{|c|cccccc|}
\hline\hline
Label          & $N_s^3\times N_t$ & $\beta$ & $a m_l$    & $a\:(\rm fm)$     & $m_\pi$ (MeV)  & $m_\pi L$\cr
\hline
\texttt{A7}  & $24^3\times 48$   & $3.31$  & $-0.09530$  & $\approx 0.116$   & $\approx 247$   & $3.6$\cr
\texttt{A8}  & $32^3\times 48$   & $3.31$  & $-0.09530$  & $\approx 0.116$   & $\approx 249$   & $4.8$\cr
\hline\hline
\end{tabular}
\end{center}
\caption{\label{tab:BMWconfigs}Parameters of the list of lattice gauge-field ensembles.}
\end{table}

\section{L{\"u}scher quantization condition}


The elasticity of the $N \pi \rightarrow (\Delta(1232)) \rightarrow N \pi$ process eases the usage of the two body L{\"u}scher quantization condition \cite{Briceno:2017max}. 
\begin{equation}
\det[M_{Jlm,J^{\prime}l^{\prime}m^{\prime}}-\delta_{JJ^{\prime}}\delta_{ll^{\prime}}\delta_{mm^{\prime}}\cot\delta_{Jl}]=0
\end{equation}

This relation connects the energy levels from a lattice simulation in a finite volume to the unknown phases $\delta_{Jl}$ in the infinite volume via the analytically known non-diagonal matrix $M_{Jlm,J'l'm'}$. It is an infinite dimensional matrix, but higher partial waves have a significantly lower contribution. It is possible then to introduce an angular momentum cut-off that makes it finite-dimensional. Furthermore, it can be block-diagonalized in the basis of lattice irreducible representations (irreps), dividing the single quantization condition into multiple independent ones that can be used to constrain the phase shift dependence.


\section{Moving frames}
Due to quantized momenta $p = 2\pi n/L$ and the fairly small volume of ensemble \texttt{A7} the energy spectrum is quite sparse which leaves only a few energies located in the energy region of interest. In order to better constrain the phase shift curve with additional points we make use of moving frames. The Lorentz boost contracts the box giving a different effective value of the spatial length $L$ \cite{leskovec2012scattering}. The situation can be also improved with the additional spectra belonging to the ensemble \texttt{A8} with larger volume, which we are adding in the near future. The list of frames used is in Table \ref{plan}.
 

\section{Angular momentum on the lattice}

In continuum, states are classified according to angular momentum $J$ and parity $P$. Those are the labels of irreducible representations in the symmetry group $SU(2)$. On the lattice the rotational symmetry is broken and the symmetry group is reduced to the cubic group $O_h$ \cite{johnson1982angular}. The 48 elements that composed the group (rotation and inversion operations) leaves the grid mesh unchanged. In this work we are dealing with fermions for which we need to consider the double cover group $O_h^D$ which have double the elements and include the $2\pi$ rotation (negative identity).
In moving frame the symmetry is further reduced to smaller groups, which gets reflected in a smaller set of irreps and a higher mixing of angular momentum in each one of them. Additionally parity do not represent a useful quantum number in boosted frame, since both parities are featured in the same irrep. In Table \ref{plan} are listed symmetry groups associated to the frames, each one with the set of irreps containing the angular momenta of interest. This many-to-one mapping of angular momentum states leads to angular momentum mixing on the lattice. This partial wave mixing is taken into account once we fit the energy levels from an irrep, through a quantization condition fit that can include multiple phase shifts with different $J$ and $L$.

%
\begin{table}
\resizebox{\textwidth}{!}{%

\begin{tabular}{l | c | c | c | c |c}
$P_{ref} [N_{dir}] $ & Group & $N_{elem}$ & $\Lambda(J):\pi ($ $0^-$ $) $& $\Lambda(J):N ($ $\frac{1}{2}^+$ $)$& $\Lambda(J):\Delta ($ $\frac{3}{2}^+$ $)$ \\
\hline \hline\\
$(0,0,0)$  $[1]$ & $O_{h}^D$  & 96 & $A_{1u}($ $0$ $,4,...)$ & $G_{1g}($ $ \frac{1}{2}$ $,\frac{7}{2},...)\oplus G_{1u}($ $ \frac{1}{2}$ $,\frac{7}{2},...)$ & $H_{g}($ $\frac{3}{2}$ $,\frac{5}{2},...) \oplus H_{u}($ $\frac{3}{2}$ $,\frac{5}{2},...)$ \\  \\
$(0,0,1)$  $[6]$ & $C_{4v}^D$ & 16 & $A_{2}($ $0$ $,1,...)$ & $G_{1}($ $ \frac{1}{2}$ $,\frac{3}{2},...)$ & $G_1(\frac{1}{2},$ $\frac{3}{2}$ $,...) \oplus G_2($ $\frac{3}{2}$ $,\frac{5}{2},...) $\\ \\
$(0,1,1)$  $[12]$ &$C_{2v}^D$ & 8 & $A_{2}($ $0$ $,1,...)$ &  $G($ $\frac{1}{2}$ $,\frac{3}{2},...)$ & $G(\frac{1}{2},$ $\frac{3}{2}$ $,...)$\\ \\
$(1,1,1)$ $[8]$ & $C_{3v}^D$ & 12 & $A_{2}($ $0$ $,1,...)$ &  $G($ $ \frac{1}{2}$ $,\frac{3}{2},...)$ & $G(\frac{1}{2},$ $\frac{3}{2}$ $,...) \oplus F_1($ $\frac{3}{2}$ $,\frac{5}{2},...) \oplus F_2($ $\frac{3}{2}$ $,\frac{5}{2},...)$

\end{tabular}
}
\caption{\label{plan}Frames, Groups $\&$ Irreps $\Lambda$ (with angular momentum content)}

\end{table}
\section{Interpolators}

For the pion we make use of the interpolator:
\begin{equation}
\pi(p)=\sum_x \bar{d}(x) \gamma_5 u(x) e^{ipx}
\end{equation}
For the nucleon the choice of interpolator is:
\begin{equation}
\begin{split}
\mathcal{N}_\mu^{(1)} (p)= \sum_x \epsilon_{abc} 
( u_{a}(x))_\mu ( u_b^T(x)C \gamma_5 d_c(x))  e^{ipx} \\
\mathcal{N}_\mu^{(2)} (p)= \sum_x \epsilon_{abc} 
(u_{a}(x))_\mu ( u_b^T(x)C\gamma_0\gamma_5 d_c(x))  e^{ipx} 
\end{split}
\end{equation}
%The former belong to the restricted Lorentz group irrep $D(1/2,0) \oplus D(0,1/2)$ and the latter to $D(1/2,1) \oplus D(1,1/2)$. 
For the Delta, we make use of:
\begin{equation}
\begin{split}
\Delta_{\mu i}^{(1)} (p)= \sum_x \epsilon_{abc} 
( u_{a}(x))_\mu ( u_b^T(x)C \gamma_i u_c(x))  e^{ipx} \\
\Delta_{\mu i}^{(2)} (p)= \sum_x \epsilon_{abc} 
( u_{a}(x))_\mu ( u_b^T(x)C \gamma_i \gamma_0 u_c(x))  e^{ipx}
\end{split}
\end{equation}

\begin{table}
\begin{center}
\begin{tabular}{c | c | c | c | c |c}
         $Irr \backslash J$    & $J=1/2$ & $J=3/2$ & $J=5/2$& $J=7/2$& $J=9/2$\\
\hline 
		 $G_{1 g/u}$ & 1 & 0 & 0 & 1 & 1   \\
	     $G_{2 g/u}$ & 0 & 0 & 1 & 1 & 0\\
	     $H_{g/u}$   & 0 & 1 & 1 & 1 & 2 \\ 
\end{tabular}
\caption{\label{spincont}Occurrence of Irreps of group $O_h^D$ (rest frame) in the subduction  of half-integer $J$ irrep of $SU(2)$ (also spin-content of irreps)} 
\end{center}


\end{table}

\section{Projection method}
\begin{figure}
\centering
\includegraphics[width=.7\textwidth]{../../Lattice18/fig/NucleonTensors.pdf}
\caption{ Tensor decomposition of the nucleon transformation matrices}
\label{Fig:nt}
\end{figure}
\begin{figure}
\centering
\includegraphics[width=.7\textwidth]{../../Lattice18/fig/DeltaTensors.pdf}
\caption{ Tensor decomposition of the Delta transformation matrices}
\label{Fig:dt}
\end{figure}
We implemented a comprehensive code package for projecting interpolators to a specific irrep $\Lambda$, row $r$ and occurence $m$ as given in the formula \cite{morningstar2013extended}
\begin{equation}
O^{\Lambda,r,m}(p)= \frac{d_\Lambda}{g_{G^D}}\sum_{R \in G^D} \Gamma_{r,r}^\Lambda (R) U_{R} \phi(p) U^{\dagger}_{R}  \;\;\;\;\; r \in \{1,\dots ,dim(\Gamma^\Lambda)\}
\label{pm}
\end{equation}
where $\Gamma^{\Lambda}$ are the representation matrices of the elements $R$ (rotations and reflections) of the double group $G^D$, $\phi(p)$ is a single/multi hadron operator (e.g. the ones presented in the section above) and $U_{R}$ are the proper transformation matrices of the chosen operator. Additionally $d_\Lambda$ is the dimension of the irrep $\Lambda$ and $g_{G^D}$ is the number of elements in the group $G^D$.

The choice of momenta $p$ establishes the relevant symmetry and the group associated with it. Looking at Table \ref{spincont} the $J$ value of our operators tell us which irrep should be contained, but to have a complete identification we can deduce them from the characters $\chi$ (trace) of the transformation matrices $U_{R}$. It is possible to find the occurence $m$ of the irrep $\Gamma^\Lambda$ in $U_{R}$ using the formula \cite{moore2006multiparticle}
\begin{equation}
m=\frac{1}{g_{G^D}}\sum_{R \in G^D} \chi^{\Gamma^\Lambda (R)}\chi^{U_R}
\label{occ}
\end{equation}
the occurrence $m$ gives us the multiplicity of the irreps for the specific operator we want to project. This correspond to the number of independent projected operators we can extract for a specific irrep $\Lambda$ and row $r$. In Fig.\ref{Fig:nt} and Fig \ref{Fig:dt} are shown schematically the decomposition of the transformation matrices $U_R$ of the nucleon and the Delta in all frames relevant for this study.



\section{Spectra}
\begin{figure}
\centering
\includegraphics[width=.48\textwidth]{StabilityFitHg+Hu_123_t1.pdf}
\includegraphics[width=.48\textwidth]{StabilityFitG1a_235_t1.pdf}
\caption{\label{Fig:stfit} Analysis of the fits($\frac{\chi}{ndf}$ listed at points). Left panel: Rest frame $\vec{P}=\frac{2\pi}{L}(0,0,0)$: group $O_h^D$ - irrep $H_g$ forward moving and $H_u$ backward moving. The ground state show a plateau at $t_{min}/a=5$ and the first excited at $t_{min}/a=4$. Right panel: Moving frame $\vec{P}=\frac{2\pi}{L}(0,0,1)$: group $C_{4v}^D$ - irrep $G_1$. For all levels it has been chosen $t_{min}/a=4$. }
\end{figure}


\begin{figure}
\centering
\includegraphics[width=.48\textwidth]{EffMassandFitHg+Hu_123_t1.pdf}
\includegraphics[width=.48\textwidth]{EffMassandFitG1a_235_t1.pdf}
\caption{\label{Fig:emp} Left panel: Rest frame $\vec{P}=\frac{2\pi}{L}(0,0,0)$: group $O_h^D$ - irrep $H_g$ forward moving and $H_u$ backward moving. The ground state has maximum overlap with the single-hadron $\Delta$-like interpolator, while the first excited level shows an expected shift in energy with respect to the first non-interacting nucleon-pion energy. Right panel: Moving frame $\vec{P}=\frac{2\pi}{L}(0,0,1)$: group $C_{4v}^D$ - irrep $G_1$. The ground state has dominant overlap to the $N\pi$ two-hadron interpolator while the second energy level overlaps with the $\Delta$ resonance . The other two levels display a shift in energy with respect to their non-interacting counterparts, gaining further distance from the resonance.}
\end{figure}


Once identified correctly the tensor decomposition in each irrep we used our code to project the $N$, $\Delta$ and $N \pi$ interpolators (the single $\pi$ do not need projection). As in \cite{morningstar2013extended}, most of the projection lead to linearly dependent operators. We made use of the Gram-Schmidt procedure to extract linear combination of operators orthogonal to each other. To maximize the statistic we made use of all the occurrences of operators, rows and momentum directions when building the correlation functions.
From this varied basis of projected interpolators we construct correlation matrices $C_{ij}^{\Lambda,r,m}$ and make use of the variational method (GEVP) \cite{Blossier:2009kd} to determine the energy spectra in each irrep. 
%
We extracted energies for all chosen irreps ($O_h^D \rightarrow H_g,H_u; C_{4v}^D\rightarrow G1,G2; C_{2v}^D\rightarrow G ; C_{3v}^D\rightarrow F1,F2,G$), but for brevity only two are shown here.
In order to find the correct energy levels removing the early-time excited states contamination, fits are performed looking for a plateau while varying the range in time. In particular they are carried out by varying $t_{min}$ at fixed $t_{max}$. In Fig. \ref{Fig:stfit}  the results of this analysis for the rest frame and the moving frame $P=2\pi/L(0,0,1)$ are shown.
In the left panel of Fig.\ref{Fig:emp} are the effective energies and fits in the chosen range of the first two energy levels for the rest frame. We perform single exponential fit directly on the principal correlators. The center of mass energies and fits for the moving frame are shown in the right panel of Fig.\ref{Fig:emp}.


\section{Conclusions}
We successfully projected all single and multi hadron operators relevant for this study, enabling the extraction of the spectrum in each chosen irrep. The result presented were done with only $1/3$ of the statistics planned for the \texttt{A7} ensemble. Furthermore, we plan to add the \texttt{A8} ensemble (Table \ref{tab:BMWconfigs}) in the near future. This two steps will give us a cleaner spectra and additional points to better constrain the phase shift curve.


\section{Acknowledgements}
We are grateful to BMW Collaboration for providing the gauge ensemble. Our calculations were performed at NERSC, supported by the U.S. DOE under Contract No. DE-AC02-05CH11231. SM and GR are supported by NSF grant PHY-1520996. SM and SS also thank the RIKEN BNL Research Center for support. JN was supported in part by the DOE Office of Nuclear Physics under grant DE-SC-0011090. AP was supported in part by the U.S. Department of Energy Office of Nuclear Physics under grant DE-FC02-06ER41444. SP is supported by the Horizon 2020 of the European Commission research and innovation programme under the Marie Sklodowska-Curie grant agreement No.642069. SK  is supported by the Deutsche Forschungsgemeinschaft grant SFB-TRR 55. We acknowledge the use of USQCD software QLUA extensively for all the calculations of correlators.




\bibliographystyle{JHEP}
\bibliography{biblioproceeding} 

\end{document}
